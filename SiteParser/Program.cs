﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using DbClassLibrary.Tables;
using DbClassLibrary.Entities;
using System.Net;

namespace SiteParser
{
    class Program
    {
        static List<TopCars> tops = new List<TopCars>();
        static public List<string> GetParseTopCarData(string url, string exUrl)
        {
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;
            string htmlCode = webClient.DownloadString(url + exUrl);

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(htmlCode);

            string node = htmlDocument.DocumentNode.SelectSingleNode("//div[@class='cols cols_percent cols_margin']//div[@class='cols__wrapper']").InnerHtml;
            htmlDocument.LoadHtml(node);

            HtmlNodeCollection nodes = htmlDocument.DocumentNode.SelectNodes("//a[@class='p-firm__text link-holder']");
            List<string> urls = new List<string>();

            string nameBrand = "";
            int count = 1;

            //DbManager.GetInstance().TableTopCars.ClearTopCars();
            foreach (HtmlNode item in nodes)
            {

                nameBrand = item.InnerText;
                if (nameBrand.Contains(" "))
                {
                    nameBrand = nameBrand.Substring(0, nameBrand.IndexOf(" "));
                }
                urls.Add(url + item.Attributes["href"].Value);

                TopCars topCars = new TopCars()
                {
                    CarBrand = nameBrand,
                    Id = count

                };
                count++;
                tops.Add(topCars);


                DbManager.GetInstance().TableTopCars.InsertTopCar(topCars);


            }
            return urls;

        }

        static public List<Car> GetParseCars(string url)
        {

            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;


            string htmlCode = webClient.DownloadString(url);

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(htmlCode);

            HtmlNodeCollection nodesName = htmlDocument.DocumentNode.SelectNodes("//div[@class='p-car p-car_catalog-firm margin_bottom_20']");

            string nameCar = "";
            string priceTemp = "";
            List<Car> carss = new List<Car>();
            string tp = "";
            int price = 0;
            // Car car = null;
            try
            {
                foreach (HtmlNode node in nodesName)
                {

                    nameCar = node.ChildNodes["a"].InnerText.Replace("'", "");
                    priceTemp = node.LastChild.InnerText;
                    if (node.LastChild.InnerText == nameCar)
                    {
                        price = 0;
                    }
                    else
                    {

                        priceTemp = priceTemp.Replace("&nbsp;", "").Replace("от", "").Replace(" ", "");
                        price = int.Parse(priceTemp);
                    }

                    Car car = new Car()
                    {
                        NameCar = nameCar,
                        Price = price,
                        TopCars = tops.Find(item => item.CarBrand == nameCar.Substring(0, nameCar.IndexOf(" "))),
                      
                    };

                    car.IdCarBrand = car.TopCars.Id;
                   
                    carss.Add(car);
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
             
            }

            return carss;

            //HtmlNodeCollection nodesName = htmlDocument.DocumentNode.SelectNodes("//a[@class='p-car__title link-holder']");
            //HtmlNodeCollection nodesPrice = htmlDocument.DocumentNode.SelectNodes($"//div[@class='p-car__text']");
        }

        static void ParseCars(List<string> urls)
        {
            List<Car> cars = new List<Car>();
            Task[] tasks = new Task[urls.Count];
            int i = 0;
            foreach (string url in urls)
            {
                tasks[i] = Task.Factory.StartNew(() =>
                {
                    cars = GetParseCars(url);
                    foreach (Car car in cars)
                    {
                        DbManager.GetInstance().TableCar.InsertCar(car);
                    }
                });
                i++;
            }
            Task.WaitAll(tasks);
        }


        static void Main(string[] args)
        {
            string exUrl = "/catalog";
            string url = "https://auto.mail.ru";
            DbManager.GetInstance().TableTopCars.ClearTopCars();
            DbManager.GetInstance().TableCar.ClearCars();
            List<string> urlss = GetParseTopCarData(url, exUrl);

            ParseCars(urlss);
            // List <Car>  cars = GetParseCars(urlss[7]);
        }



    }
}
