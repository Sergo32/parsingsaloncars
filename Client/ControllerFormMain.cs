﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbClassLibrary.Entities;
using DbClassLibrary.Tables;
using System.Windows.Forms;

namespace Client
{

    class ControllerFormMain
    {
        private FormMain form;
        private DbManager dbManager;


        public ControllerFormMain(FormMain form)
        {
            this.form = form;
            dbManager = DbManager.GetInstance();
        }

        public void FillComboBoxTopCar()
        {
            form.comboBoxTopCar.DataSource = null;
            form.comboBoxTopCar.DataSource = dbManager.TableTopCars.GetAllTopCars();
        }

        public void FillDataGridCars()
        {
            form.dataGridViewCars.DataSource = null;
            form.dataGridViewCars.DataSource = dbManager.TableCar.GetAllCars();
            // form.dataGridViewStudents.Columns["Id_debts"].Visible = false;

        }

        public void ClearFields()
        {
            form.comboBoxTopCar.SelectedItem = -1;
        }

        public void LoadCars()
        {
            TopCars top = (TopCars)form.comboBoxTopCar.SelectedItem;
            form.dataGridViewCars.DataSource = null;
            form.dataGridViewCars.DataSource = dbManager.TableCar.GetAllCarsIdMark(top.Id);
            form.dataGridViewCars.Columns["TopCars"].Visible = false;
            form.dataGridViewCars.Columns["IdCarBrand"].Visible = false;
        }

        public void LoadAllCars()
        {
            TopCars top = (TopCars)form.comboBoxTopCar.SelectedItem;
            form.dataGridViewCars.DataSource = null;
            form.dataGridViewCars.DataSource = dbManager.TableCar.GetAllCars();
            form.dataGridViewCars.Columns["TopCars"].Visible = false;
            form.dataGridViewCars.Columns["IdCarBrand"].Visible = false;
        }


    }


}
