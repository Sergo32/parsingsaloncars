﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;




namespace Client
{
    public partial class FormMain : Form
    {
        private ControllerFormMain controller;
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controller = new ControllerFormMain(this);

            Thread.Sleep(3000);
            controller.FillComboBoxTopCar();                       
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            controller.LoadCars();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            controller.LoadAllCars();
        }
    }
}
