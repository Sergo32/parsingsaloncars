﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using DbClassLibrary.Entities;

namespace DbClassLibrary.Tables
{
    public class TableCar
    {
        private string connectionString;

        public TableCar(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Car> GetAllCarsIdMark(int id)
        {
            List<Car> cars = null;
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"Select * From `car` where `id_carBrand`={id}";

                        MySqlDataReader reader = mySqlCommand.ExecuteReader();

                        cars = new List<Car>();

                        while (reader.Read())
                        {
                            Car car = new Car()
                            {
                                Id = reader.GetInt32("id"),
                                Price = reader.GetInt32("price"),
                                IdCarBrand = reader.GetInt32("id_carBrand"),
                                NameCar = reader.GetString("nameCar")
                            };


                            cars.Add(car);
                        }
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }

            return cars;
        }
        public List<Car> GetAllCars()
        {
            List<Car> cars = null;
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = "Select * From `car`";

                        MySqlDataReader reader = mySqlCommand.ExecuteReader();

                        cars = new List<Car>();

                        while (reader.Read())
                        {
                            Car car = new Car()
                            {
                                Id = reader.GetInt32("id"),
                                Price = reader.GetInt32("price"),
                                IdCarBrand = reader.GetInt32("id_carBrand"),
                                NameCar = reader.GetString("nameCar")
                            };

                            
                            cars.Add(car);
                        }
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }

            return cars;
        }

        public void ClearCars()
        {
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = "Truncate Table `car`";
                        mySqlCommand.ExecuteNonQuery();
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }

        }

        public Car GetCarByName(string nameCar)
        {
            Car car = null;
            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"Select * From `car` where `name`={nameCar}";
                        MySqlDataReader reader = mySqlCommand.ExecuteReader();
                        reader.Read();
                        car = new Car()
                        {
                            Id = reader.GetInt32("id"),

                            Price = reader.GetInt32("price"),
                            IdCarBrand = reader.GetInt32("id_carBrand"),
                            NameCar = reader.GetString("nameCar")


                        };


                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }

            return car;
        }

        public void InsertCar(Car car)
        {

            try
            {
                using (MySqlConnection mySqlConnection = new MySqlConnection(connectionString))
                {
                    mySqlConnection.Open();

                    using (MySqlCommand mySqlCommand = new MySqlCommand())
                    {
                        mySqlCommand.Connection = mySqlConnection;
                        mySqlCommand.CommandText = $"INSERT INTO `car` (`id_carBrand`,`nameCar`,`price` ) VALUES ({car.IdCarBrand},'{car.NameCar}',{car.Price});";
                        mySqlCommand.ExecuteNonQuery();
                    }

                    mySqlConnection.Close();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                throw;
            }
        }
    }

}
