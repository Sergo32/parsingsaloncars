﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbClassLibrary.Tables
{
   public class DbManager
    {
        private static DbManager instance = null;

        public static DbManager GetInstance()
        {
            if (instance==null)
            {
                instance = new DbManager();
            }

            return instance;
        }

        public TableCar TableCar { get; private set; }
        public TableTopCars  TableTopCars { get; private set; }

        public DbManager()
        {
            string connectionString = "Server=localhost;User=root;Password=1234;Database=topcarssite";

            TableCar = new TableCar(connectionString);
            TableTopCars = new TableTopCars(connectionString);

        }
    }
}
