﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbClassLibrary.Entities
{
    public class TopCars
    {
        public int Id { get; set; }
        public string CarBrand { get; set; }

        public override string ToString()
        {
            return CarBrand;
        }
    }
}
